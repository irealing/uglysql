package cn.fuser.uglysql

import cn.fuser.uglysql.sql.Field
import cn.fuser.uglysql.sql.Filter
import cn.fuser.uglysql.sql.SQLQuery
import cn.fuser.uglysql.sql.Table

open class DBQuery<T>(private val session: DBSession, table: Table, private vararg val fields: Field<T>) {
    private val sqlQuery = SQLQuery(table, *fields)
    fun <E> leftJoin(table: Table, on: Filter<E>): DBQuery<T> = apply { sqlQuery.leftJoin(table, on) }
    fun <E> innerJoin(table: Table, on: Filter<E>): DBQuery<T> = apply { sqlQuery.innerJoin(table, on) }
    fun <E> rightJoin(table: Table, on: Filter<E>): DBQuery<T> = apply { sqlQuery.rightJoin(table, on) }
    fun where(vararg fs: Filter<*>): DBQuery<T> = apply { sqlQuery.where(*fs) }
    fun or(filter: Filter<*>, vararg fs: Filter<*>): DBQuery<T> = apply { sqlQuery.or(filter, *fs) }
    private fun execute() {
        val ps = session.prepareStatement(sqlQuery.sql())
        ps.execute()
    }

    fun one() {
    }

    fun all() {
    }
}