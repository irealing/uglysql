package cn.fuser.uglysql

import cn.fuser.uglysql.sql.Table

data class Mapping<E>(val schema: Table, val entry: E)
