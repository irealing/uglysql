package cn.fuser.uglysql

import cn.fuser.uglysql.sql.Field
import cn.fuser.uglysql.sql.Table
import java.sql.Connection
import java.sql.PreparedStatement

class DBSession private constructor(private val connection: Connection) : AutoCloseable {
    companion object {
        fun start(connect: () -> Connection): DBSession = DBSession(connect.invoke())
    }

    override fun close() {
        this.connection.close()
    }

    fun prepareStatement(sql: String): PreparedStatement {
        return connection.prepareStatement(sql)
    }

    fun <T> query(table: Table, vararg fs: Field<T>): DBQuery<T> = DBQuery(this, table, *fs)
}