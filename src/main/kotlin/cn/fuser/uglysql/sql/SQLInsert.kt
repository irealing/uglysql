package cn.fuser.uglysql.sql
import java.util.*

/**
 * [SQLInsert] SQL `INSERT`语句
 * */
class SQLInsert(private val table: Table) : SQLFragment<Any?> {
    private data class FieldValuePair<T>(val field: Field<T>, val value: T)

    private val pairs = LinkedList<FieldValuePair<*>>()
    /**
     * [set] 设置字段值
     * */
    fun <T> set(field: Field<T>, value: T): SQLInsert = apply { pairs.add(FieldValuePair(field, value)) }

    override fun sql(): String {
        val keysJoiner = StringJoiner(Const.SYMBOL_COMMA)
        keysJoiner.addAll(pairs) { it.field.name }
        val placeHolderJoiner = StringJoiner(Const.SYMBOL_COMMA)
        placeHolderJoiner.addAll(Collections.nCopies(pairs.size, Const.SYMBOL_PLACEHOLDER)).toString()

        val symbols = arrayOf(
            Const.KEY_INSERT, Const.KEY_INTO, table.sql(), Const.SYMBOL_L_BRACKET,
            keysJoiner.toString(), Const.SYMBOL_R_BRACKET, Const.KEY_VALUES,
            Const.SYMBOL_L_BRACKET, placeHolderJoiner.toString(), Const.SYMBOL_R_BRACKET
        )
        return StringJoiner(Const.SYMBOL_SPACE).addAll(symbols).toString()
    }

    override fun params(): List<Any?> {
        val ret = LinkedList<Any?>()
        pairs.forEach { ret.add(it.value) }
        return ret
    }
}

