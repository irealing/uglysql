package cn.fuser.uglysql.sql
import java.util.*

/**
 * [SQLUpdate] 数据库UPDATE语句
 * */
class SQLUpdate(private val table: Table) : SQLFragment<Any?> {
    private val pairs: MutableList<Filter<*>> = LinkedList()
    private val where: Where = Where()
    private val joinGroup = JoinGroup()
    fun <T> set(field: Field<T>, value: T): SQLUpdate {
        pairs.add(field.eq(value))
        return this
    }

    fun <T> set(field: Field<T>, other: Field<T>): SQLUpdate = apply { pairs.add(field.eq(other)) }

    fun where(vararg filters: Filter<*>): SQLUpdate = apply { where.where(*filters) }

    fun or(filter: Filter<*>, vararg fs: Filter<*>): SQLUpdate = apply { where.where(filter, *fs) }


    fun leftJoin(table: Table, filter: Filter<*>): SQLUpdate = apply { joinGroup.leftJoin(table, filter) }
    fun innerJoin(table: Table, filter: Filter<*>): SQLUpdate = apply { joinGroup.innerJoin(table, filter) }
    fun rightJoin(table: Table, filter: Filter<*>): SQLUpdate = apply { joinGroup.rightJoin(table, filter) }
    override fun params(): List<Any?> {
        val retList = LinkedList<Any?>()
        pairs.forEach { retList.addAll(it.params()) }
        retList.addAll(where.params())
        return retList
    }

    override fun sql(): String {
        val pairsSJ = StringJoiner(Const.SYMBOL_COMMA).addAll(pairs) { it.sql() }.toString()
        val elem = arrayOf(Const.KEY_UPDATE, joinGroup.sql(), Const.KEY_SET, table.sql(), pairsSJ, where.sql())
        return StringJoiner(Const.SYMBOL_SPACE).addAll(elem).toString()
    }
}