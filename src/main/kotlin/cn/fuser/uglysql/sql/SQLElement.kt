package cn.fuser.uglysql.sql
/**
 * [SQLElement] SQL元素
 * */
interface SQLElement {
    /**
     * [sql] 生成SQL语句
     * */
    fun sql(): String
}

/**
 * [SQLFragment] SQL片段
 * */
interface SQLFragment<T> : SQLElement {
    /**
     * [params] SQL语句参数列表
     * */
    fun params(): List<T>
}

interface Filter<T> : SQLFragment<T>
