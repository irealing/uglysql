package cn.fuser.uglysql.sql

import java.util.*

inline fun <T> StringJoiner.addAll(objs: Collection<T>, func: (o: T) -> String): StringJoiner {
    objs.forEach { this.add(func.invoke(it)) }
    return this
}

fun StringJoiner.addAll(ss: Collection<String>): StringJoiner = this.apply { ss.forEach { this.add(it) } }

fun StringJoiner.addAll(ss: Array<String>): StringJoiner = this.apply { ss.forEach { this.add(it) } }

inline fun <E> StringJoiner.addAll(ss: Array<E>, func: (E) -> String): StringJoiner {
    ss.forEach { this.add(func.invoke(it)) }
    return this
}

inline fun <E, T> LinkedList<E>.wraps(func: (E) -> Collection<T>): List<T> {
    val retList = LinkedList<T>()
    this.forEach { retList.addAll(func.invoke(it)) }
    return retList
}