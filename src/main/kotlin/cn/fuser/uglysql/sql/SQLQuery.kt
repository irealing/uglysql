package cn.fuser.uglysql.sql
import java.util.*

class SQLQuery(table: Table, vararg field: Field<*>) : SQLFragment<Any?> {
    private val select = Select(table, *field)
    private val filters = Where()
    private val joinGroup = JoinGroup()
    override fun sql(): String = StringJoiner(Const.SYMBOL_SPACE).addAll(elem()) { it.sql() }.toString()
    private fun elem(): Collection<SQLElement> {
        return listOf(select, joinGroup, filters)
    }

    override fun params(): List<Any?> = filters.params()

    fun where(vararg fs: Filter<*>): SQLQuery {
        filters.where(*fs)
        return this
    }

    fun or(f: Filter<*>, vararg fs: Filter<*>): SQLQuery {
        filters.or(f, *fs)
        return this
    }

    fun <T> leftJoin(table: Table, on: Filter<T>): SQLQuery {
        joinGroup.leftJoin(table, on)
        return this
    }

    fun <T> innerJoin(table: Table, on: Filter<T>): SQLQuery {
        joinGroup.innerJoin(table, on)
        return this
    }

    fun <T> rightJoin(table: Table, on: Filter<T>): SQLQuery {
        joinGroup.rightJoin(table, on)
        return this
    }
}