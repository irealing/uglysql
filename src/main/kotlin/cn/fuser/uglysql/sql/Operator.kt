package cn.fuser.uglysql.sql
enum class Operator(val opt: String) : SQLElement {
    EQ(Const.SYMBOL_EQ),
    LT(Const.SYMBOL_LT),
    LE(Const.SYMBOL_LE),
    GT(Const.SYMBOL_GT),
    GE(Const.SYMBOL_GE),
    IN(Const.SYMBOL_IN);

    override fun sql(): String = opt
}