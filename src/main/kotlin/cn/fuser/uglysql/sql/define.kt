package cn.fuser.uglysql.sql
import java.util.*

abstract class Table : SQLElement {
    abstract val tableName: String
    override fun toString(): String = sql()
    override fun sql(): String = "`%s`".format(tableName)
}

/**
 * [BaseFilter] 基础过滤器
 * */
private abstract class BaseFilter<T> : Filter<T> {
    override fun toString(): String = sql()
}

private class SimpleFilter<T>(private val field: Field<T>, private val opt: Operator, private val v: T) :
    BaseFilter<T>() {
    override fun sql(): String = "%s %s ?".format(field.sql(), opt.opt)

    override fun params(): List<T> = listOf(v)
}

private class CompareFilter<T>(private val field: Field<T>, private val opt: Operator, private val other: Field<T>) :
    BaseFilter<T>() {
    override fun sql(): String = "%s %s %s".format(field.sql(), opt.opt, other.sql())

    override fun params(): List<T> = listOf()
}

private class InFilter<T>(private val field: Field<T>, private val args: List<T>) : BaseFilter<T>() {
    override fun sql(): String {
        val sj = StringJoiner(Const.SYMBOL_COMMA)
        Collections.nCopies(args.size, Const.SYMBOL_PLACEHOLDER).forEach { sj.add(it) }
        return "%s %s (%s)".format(field.sql(), Operator.IN.opt, sj.toString())
    }

    override fun params(): List<T> = args
}

open class Field<T>(val table: Table, val name: String) : SQLElement {
    override fun sql(): String = "%s.`%s`".format(table.sql(), name)
    fun eq(value: T): Filter<T> = SimpleFilter(this, Operator.EQ, value)
    fun eq(other: Field<T>): Filter<T> = CompareFilter(this, Operator.EQ, other)
    fun lt(value: T): Filter<T> = SimpleFilter(this, Operator.LT, value)
    fun lt(other: Field<T>): Filter<T> = CompareFilter(this, Operator.LT, other)
    fun le(value: T): Filter<T> = SimpleFilter(this, Operator.LE, value)
    fun le(other: Field<T>): Filter<T> = CompareFilter(this, Operator.LE, other)
    fun gt(value: T): Filter<T> = SimpleFilter(this, Operator.GT, value)
    fun gt(other: Field<T>): Filter<T> = CompareFilter(this, Operator.GT, other)
    fun ge(value: T): Filter<T> = SimpleFilter(this, Operator.GE, value)
    fun ge(other: Field<T>): Filter<T> = CompareFilter(this, Operator.GE, other)
    fun In(vararg values: T): Filter<T> = InFilter(this, values.asList())
    fun asc(): OrderBy<T> = OrderBy(this)
    fun desc(): OrderBy<T> = OrderBy(this, OrderBy.Order.DESC)
}

class OrFilter(first: Filter<*>, vararg fs: Filter<*>) : Filter<Any> {
    private val filters = LinkedList<Filter<*>>()

    init {
        filters.add(first)
        filters.addAll(fs)
    }

    override fun sql(): String {
        val sj = StringJoiner(Const.SYMBOL_SPACE + Const.KEY_OR + Const.SYMBOL_SPACE)
        sj.addAll(filters) { it.sql() }
        return sj.toString()
    }

    override fun params(): List<Any> = filters.wraps { it.params().filterNotNull() }
}

class FuncField<T> private constructor(private val field: Field<T>, private val func: String) :
    Field<T>(field.table, field.name) {
    override fun sql(): String = "%s(%s)".format(func.toUpperCase(), field.sql())

    companion object {
        private fun <T> funcField(field: Field<T>, func: String) = FuncField(field, func)
        fun <T> max(field: Field<T>): Field<T> = funcField(field, Const.FUNC_MAX)
        fun <T> min(field: Field<T>): Field<T> = funcField(field, Const.FUNC_MIN)
        fun <T> count(field: Field<T>): Field<T> = funcField(field, Const.FUNC_COUNT)
        fun <T> sum(field: Field<T>): Field<T> = funcField(field, Const.FUNC_SUM)
        fun <T> avg(field: Field<T>): Field<T> = funcField(field, Const.FUNC_AVG)
    }
}