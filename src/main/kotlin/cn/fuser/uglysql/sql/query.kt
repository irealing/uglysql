package cn.fuser.uglysql.sql
import java.util.*

class Select(private val table: Table, private vararg val col: Field<*>) : SQLElement {
    override fun sql(): String {
        val sj = StringJoiner(Const.SYMBOL_SPACE)
        val csj = StringJoiner(Const.SYMBOL_COMMA)
        csj.addAll(col) { it.sql() }
        sj.addAll(arrayOf(Const.KEY_SELECT, csj.toString(), Const.KEY_FROM, table.sql()))
        return sj.toString()
    }
}

class Where(vararg fs: Filter<*>) : SQLFragment<Any?> {
    private val filters: MutableList<Filter<*>> = LinkedList(fs.asList())

    override fun sql(): String {
        val sj = StringJoiner(Const.SYMBOL_SPACE + Const.KEY_AND + Const.SYMBOL_SPACE)
        sj.addAll(filters) { it.sql() }
        return StringJoiner(Const.SYMBOL_SPACE).addAll(arrayOf(Const.KEY_WHERE, sj.toString())).toString()
    }

    fun where(vararg fs: Filter<*>): Where {
        this.filters.addAll(fs)
        return this
    }

    override fun params(): List<Any?> {
        val args = LinkedList<Any?>()
        filters.forEach {
            args.addAll(it.params())
        }
        return args
    }

    fun or(f: Filter<*>, vararg fs: Filter<*>): Where {
        filters.add(OrFilter(f, *fs))
        return this
    }
}

class OrderBy<T>(private val field: Field<T>, private val order: Order) : SQLElement {
    constructor(field: Field<T>) : this(field, Order.ASC)

    enum class Order(val order: String) {
        ASC(Const.KEY_ASC), DESC(Const.KEY_DESC)
    }

    override fun sql(): String = "ORDER BY %s %s".format(field.sql(), order.order)
}

class Join<T> private constructor(private val table: Table, private val method: JoinMethod, private val on: Filter<T>) :
    SQLElement {
    private enum class JoinMethod(val method: String) {
        LEFT(Const.KEY_LEFT_JOIN), INNER(Const.KEY_INNER_JOIN), RIGHT(Const.KEY_RIGHT_JOIN)
    }

    companion object {
        fun <T> innerJoin(table: Table, filter: Filter<T>) = Join(table, JoinMethod.INNER, filter)
        fun <T> leftJoin(table: Table, filter: Filter<T>) = Join(table, JoinMethod.LEFT, filter)
        fun <T> rightJoin(table: Table, filter: Filter<T>) = Join(table, JoinMethod.RIGHT, filter)
    }

    override fun sql(): String {
        return StringJoiner(Const.SYMBOL_SPACE).addAll(
            arrayOf(
                method.method,
                table.tableName,
                Const.KEY_ON,
                on.sql()
            )
        ).toString()
    }
}

class JoinGroup : SQLElement {
    private val joins = LinkedList<Join<*>>()
    fun <T> leftJoin(table: Table, filter: Filter<T>): JoinGroup {
        joins.add(Join.leftJoin(table, filter))
        return this
    }

    fun <T> rightJoin(table: Table, filter: Filter<T>): JoinGroup {
        joins.add(Join.rightJoin(table, filter))
        return this
    }

    fun <T> innerJoin(table: Table, filter: Filter<T>): JoinGroup {
        joins.add(Join.innerJoin(table, filter))
        return this
    }

    override fun sql(): String {
        return if (joins.size < 1) return Const.EMPTY_STRING else StringJoiner(Const.SYMBOL_SPACE).addAll(joins) { it.sql() }.toString()
    }

    override fun toString(): String = sql()
}