package cn.fuser.uglysql.sql;

public final class Const {
    public final static String SYMBOL_PLACEHOLDER = "?";
    public final static String SYMBOL_COMMA = ",";
    public final static String EMPTY_STRING = "";
    public final static String SYMBOL_SPACE = " ";
    public final static String SYMBOL_EQ = "=";
    public final static String SYMBOL_LT = "<";
    public final static String SYMBOL_LE = "<=";
    public final static String SYMBOL_GT = ">";
    public final static String SYMBOL_GE = ">=";
    public final static String SYMBOL_IN = "IN";
    public final static String SYMBOL_L_BRACKET = "(";
    public final static String SYMBOL_R_BRACKET = ")";
    public final static String KEY_INTO = "INTO";
    public final static String KEY_FROM = "FROM";
    public final static String KEY_SELECT = "SELECT";
    public final static String KEY_AND = "AND";
    public final static String KEY_VALUES = "VALUES";
    public final static String KEY_UPDATE = "UPDATE";
    public final static String KEY_INSERT = "INSERT";
    public final static String KEY_DELETE = "DELETE";
    public final static String KEY_WHERE = "WHERE";
    public final static String KEY_OR = "OR";
    public final static String KEY_ASC = "ASC";
    public final static String KEY_DESC = "DESC";
    public final static String FUNC_MAX = "MAX";
    public final static String FUNC_MIN = "MIN";
    public final static String FUNC_COUNT = "COUNT";
    public final static String FUNC_SUM = "SUM";
    public final static String FUNC_AVG = "AVG";
    public final static String KEY_INNER_JOIN = "INNER JOIN";
    public final static String KEY_LEFT_JOIN = "LEFT JOIN";
    public final static String KEY_RIGHT_JOIN = "RIGHT JOIN";
    public final static String KEY_ON = "ON";
    public final static String KEY_SET = "SET";

    private Const() {
    }
}
